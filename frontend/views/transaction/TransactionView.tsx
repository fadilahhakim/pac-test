import Modal from "Frontend/components/modal/Modal";
import {useEffect, useState} from "react";
import Transaction from "Frontend/generated/com/example/application/Entity/Transaction";
import {TransactionService} from "Frontend/generated/endpoints";
import {Grid} from "@hilla/react-components/Grid";
import {GridColumn} from "@hilla/react-components/GridColumn";

export default function TransactionView () {

    const [transactions, setTransactions] = useState<Transaction[]>([]);

    useEffect(() =>{
        fetchTransactions();
    },[]);
    const fetchTransactions = async () => {
        try {
            const result = await TransactionService.getAllTransaction();
            setTransactions(result);
            console.log(result);
        } catch (error) {
            console.error('Error fetching transactions:', error);
        }
    };

    function renderTableTransaction() {
        return(
            <>
                <Grid items={transactions}>
                    <GridColumn path="id"></GridColumn>
                    <GridColumn path="date"></GridColumn>
                    <GridColumn path="type"></GridColumn>
                    <GridColumn path="amount"></GridColumn>
                    <GridColumn path="balance"></GridColumn>
                </Grid>
            </>
        )
    }


    return(
        <>
           <h2>Transaction Page</h2>
           <Modal />

            <h2>Transaction History</h2>

            <Grid items={transactions}>
                <GridColumn path="id"></GridColumn>
                <GridColumn path="date"></GridColumn>
                <GridColumn path="type"></GridColumn>
                <GridColumn path="amount"></GridColumn>
                <GridColumn path="debitKredit"></GridColumn>
                <GridColumn path="balance"></GridColumn>
            </Grid>


        </>
    );

}