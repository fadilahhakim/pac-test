import React, {useEffect, useState} from 'react';
import { Dialog } from '@hilla/react-components/Dialog.js';
import { Button } from '@hilla/react-components/Button.js';
import { TextField } from '@hilla/react-components/TextField.js';
import { VerticalLayout } from '@hilla/react-components/VerticalLayout.js';
import { TextArea } from '@hilla/react-components/TextArea.js';
import {Select} from "@hilla/react-components/Select";
import Transaction from "Frontend/generated/com/example/application/Entity/Transaction";
import TransactionType from "Frontend/generated/com/example/application/Entity/TransactionType";
import {TransactionService} from "Frontend/generated/endpoints";
export default function Modal() {
    const [dialogOpened, setDialogOpened] = useState(false);

    const [amount, setAmount] = useState<number>(0);
    const [selectedValue, setSelectedValue] = useState<string>('');

    const trxType = [
        {
            label: 'WITHDRAW',
            value: 'WITHDRAW',
        },
        {
            label: 'DEPOSIT',
            value: 'DEPOSIT',
        },{
            label: 'TRANSFER',
            value: 'TRANSFER',
        }
    ];


    const open = () => {
        setDialogOpened(true);
    };

    const close = () => {
        setDialogOpened(false);
    };
    const toTrx = () => {
        switch (selectedValue) {
            case "WITHDRAW":
                TransactionService.withdraw(amount)
                    .then(() => {
                        alert("Success WD");
                        showLoading();
                        setTimeout(() => window.location.reload(), 2000);
                    })
                    .catch((error) => {
                        console.error("Withdrawal failed:", error);
                        alert("Withdrawal failed. Please try again.");
                    });
                break;
            case "DEPOSIT":
                TransactionService.deposit(amount)
                    .then(() => {
                        alert("Success Om");
                        showLoading();
                        setTimeout(() => window.location.reload(), 2000);
                    })
                    .catch((error) => {
                        console.error("Deposit failed:", error);
                        alert("Deposit failed. Please try again.");
                    });
                break;
            case "TRANSFER":
                TransactionService.transfer(amount)
                    .then(() => {
                        alert("Success Transfer");
                        showLoading();
                        setTimeout(() => window.location.reload(), 2000);
                    })
                    .catch((error) => {
                        console.error("Transfer failed:", error);
                        alert("Transfer failed. Please try again.");
                    });
                break;
            default:
                alert("Internal Server Error");
        }
    };

    const showLoading = () => {
        console.log("Loading...");
    };



    // @ts-ignore
    return (

        <>
            <Dialog
                aria-label="Bank Dil"
                draggable
                modeless
                opened={dialogOpened}
                onOpenedChanged={(event) => {
                    setDialogOpened(event.detail.value);
                }}
                headerRenderer={() => (
                    <h2
                        className="draggable"
                        style={{
                            flex: 1,
                            cursor: 'move',
                            margin: 0,
                            fontSize: '1.5em',
                            fontWeight: 'bold',
                            padding: 'var(--lumo-space-m) 0',
                        }}
                    >
                        Bank Dil
                    </h2>
                )}
                footerRenderer={() => (
                    <>
                        <Button onClick={close}>Cancel</Button>
                        <Button
                            theme="primary"
                            onClick = {toTrx}
                        >
                           Do Transaction
                        </Button>
                    </>
                )}
            >
                <VerticalLayout
                    theme="spacing"
                    style={{ width: '300px', maxWidth: '100%', alignItems: 'stretch' }}
                >
                    <VerticalLayout style={{ alignItems: 'stretch' }}>
                        <Select
                            label="Transaction Type"
                            onValueChanged={(e) => {
                                setSelectedValue(e.detail.value);
                            }}
                            placeholder="Transaction Type" items={trxType} />
                        <TextArea
                            label="Amount"
                            onValueChanged={(e) => {
                            setAmount(parseInt(e.detail.value));
                        }}/>
                    </VerticalLayout>
                </VerticalLayout>
            </Dialog>

            <Button
                onClick={open}
            >
                Mesin ATM
            </Button>
        </>
    );
}