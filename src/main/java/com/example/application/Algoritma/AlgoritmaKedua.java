package com.example.application.Algoritma;

public class AlgoritmaKedua {

    public static void main(String[] args) {
        String input = "saya sedang belajar berenang";
        String output = reverseString(input);
        System.out.println("Input data: " + input);
        System.out.println("Output data: " + output);
    }

    static String reverseString(String input) {
        StringBuilder result = new StringBuilder();

        String[] stringRevers = input.split("\\s+");

        for (String text : stringRevers) {
            StringBuilder output = new StringBuilder();
            for (int i = text.length() - 1; i >= 0; i--) {
                output.append(text.charAt(i));
            }
            result.append(output).append(" ");
        }
        return result.toString().trim();
    }
}
