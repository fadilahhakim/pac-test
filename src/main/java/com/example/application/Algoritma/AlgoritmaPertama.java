package com.example.application.Algoritma;

public class AlgoritmaPertama {
    public static void main(String[] args) {
        String input = "Saya SeDAng beLaJar BaHasa PemrOrgRaman JAVA";
        String output = reverseString(input);
        System.out.println("Input data: " + input);
        System.out.println("Output data: " + output);
    }

    static String reverseString(String input) {
        StringBuilder result = new StringBuilder();

        String[] userInput = input.split("\\s+");

        for (String output : userInput) {
            StringBuilder stringRevers = new StringBuilder();
            for(int i = 0; i < output.length(); i++) {
                char karakter = output.charAt(i);
                if (Character.isUpperCase(karakter)) {
                    stringRevers.append(Character.toLowerCase(karakter));
                }else {
                    stringRevers.append(Character.toUpperCase(karakter));
                }
            }
            result.append(stringRevers).append(" ");
        }

        return result.toString().trim();
    }
}
