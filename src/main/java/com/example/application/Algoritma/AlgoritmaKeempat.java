package com.example.application.Algoritma;

import java.util.HashMap;
import java.util.Map;

public class AlgoritmaKeempat {

    public static void main(String[] args) {
        String teks = "Java adalah bahasa pemrograman yang dapat dijalankan di berbagai komputer termasuk telepon genggam. Bahasa ini awalnya dibuat oleh James Gosling saat masih bergabung di Sun Microsystems, yang saat ini merupakan bagian dari Oracle dan dirilis tahun 1995. Bahasa ini banyak mengadopsi sintaksis yang terdapat pada C dan C++ namun dengan sintaksis model objek yang lebih sederhana serta dukungan rutin-rutin aras bawah yang minimal. Aplikasi-aplikasi berbasis java umumnya dikompilasi ke dalam bytecode dan dapat dijalankan pada berbagai Mesin Virtual Java.\n\nJava merupakan bahasa pemrograman yang bersifat umum dan secara khusus didesain untuk memanfaatkan dependensi implementasi seminimal mungkin. Karena fungsionalitasnya yang memungkinkan aplikasi java mampu berjalan di beberapa platform sistem operasi yang berbeda, java dikenal pula dengan slogannya, Tulis sekali, jalankan di mana pun. Saat ini java merupakan bahasa pemrograman yang paling populer digunakan, dan secara luas dimanfaatkan dalam pengembangan berbagai jenis perangkat lunak";

        String[] kataKata = teks.toLowerCase().split("\\s+");

        int jumlahKata = kataKata.length;
        System.out.println("Jumlah kata: " + jumlahKata + "\n");

        Map<String, Integer> frekuensiKata = new HashMap<>();
        for (String kata : kataKata) {
            frekuensiKata.put(kata, frekuensiKata.getOrDefault(kata, 0) + 1);
        }

        System.out.println("Jumlah kemunculan tiap kata:");
        frekuensiKata.forEach((kata, kemunculan) -> {
            System.out.println(kata + ": " + kemunculan);
        });
        System.out.println();

        int jumlahKataSatuKali = (int) frekuensiKata.values().stream().filter(value -> value == 1).count();
        System.out.println("Jumlah kata yang muncul hanya satu kali: " + jumlahKataSatuKali + "\n");

        int maxKemunculan = frekuensiKata.values().stream().max(Integer::compare).orElse(0);
        System.out.println("Kata yang paling banyak muncul:");
        frekuensiKata.forEach((kata, kemunculan) -> {
            if (kemunculan == maxKemunculan) {
                System.out.println(kata + ": " + kemunculan + " kali");
            }
        });
        System.out.println();

        int minKemunculan = frekuensiKata.values().stream().min(Integer::compare).orElse(0);
        System.out.println("Kata yang paling sedikit muncul:");
        frekuensiKata.forEach((kata, kemunculan) -> {
            if (kemunculan == minKemunculan) {
                System.out.println(kata + ": " + kemunculan + " kali");
            }
        });
    }
}
