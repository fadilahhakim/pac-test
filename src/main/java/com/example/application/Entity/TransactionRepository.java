package com.example.application.Entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * FROM dummy_transaction t ORDER BY t.date DESC LIMIT 1", nativeQuery = true)
    Transaction findLastTransaction();

    default BigDecimal getLastBalance() {
        Transaction latestTransaction = findLastTransaction();

        return (latestTransaction != null) ? latestTransaction.getBalance() : new BigDecimal(1000000);
    }
}
