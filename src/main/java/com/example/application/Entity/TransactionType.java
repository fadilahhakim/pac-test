package com.example.application.Entity;

public enum TransactionType {
    WITHDRAW("Tarik Tunai"),
    DEPOSIT("Setor Tunai"),
    TRANSFER("Transfer");

    private final String description;
    TransactionType(String description) {
        this.description = description;
    }

    public String getdescription() {
        return description;
    }
}
