package com.example.application.services;

import com.example.application.Entity.Transaction;
import com.example.application.Entity.TransactionRepository;
import com.example.application.Entity.TransactionType;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import dev.hilla.BrowserCallable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@AnonymousAllowed
@BrowserCallable
@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> getAllTransaction(){
        return transactionRepository.findAll();
    }


    public void withdraw(BigDecimal amount) {
        Transaction trx = new Transaction();
        trx.setType(TransactionType.WITHDRAW);
        trx.setDate(new Date());
        trx.setDebitKredit("DEBIT");

        BigDecimal afterWithdraw = transactionRepository.getLastBalance().subtract(amount);
        trx.setBalance(afterWithdraw);
        trx.setAmount(amount);

        transactionRepository.save(trx);
    }

    public void transfer(BigDecimal amount) {
        Transaction trx = new Transaction();
        trx.setType(TransactionType.TRANSFER);
        trx.setDate(new Date());
        trx.setDebitKredit("DEBIT");

        BigDecimal afterTransfer = transactionRepository.getLastBalance().subtract(amount);
        trx.setBalance(afterTransfer);
        trx.setAmount(amount);


        transactionRepository.save(trx);
    }

    public void deposit(BigDecimal amount) {
        Transaction trx = new Transaction();
        trx.setType(TransactionType.DEPOSIT);
        trx.setDate(new Date());
        trx.setDebitKredit("KREDIT");

        BigDecimal afterDeposit = transactionRepository.getLastBalance().add(amount);
        trx.setBalance(afterDeposit);
        trx.setAmount(amount);

        transactionRepository.save(trx);
    }


}
