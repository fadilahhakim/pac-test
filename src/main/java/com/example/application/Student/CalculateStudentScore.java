package com.example.application.Student;

import java.util.Scanner;

public class CalculateStudentScore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan Jumlah Mahasiswa: ");
        int jumlahMahasiswa = scanner.nextInt();

        int[] nimArray = new int[jumlahMahasiswa];
        String[] namaArray = new String[jumlahMahasiswa];
        double[] nilaiAkhirArray = new double[jumlahMahasiswa];
        char[] gradeArray = new char[jumlahMahasiswa];

        int jumlahLulus = 0;
        int jumlahTidakLulus = 0;

        for (int i = 0; i < jumlahMahasiswa; i++) {
            System.out.println("\nData Mahasiswa ke-" + (i + 1));
            System.out.print("Masukkan NIM: ");
            nimArray[i] = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Masukkan Nama: ");
            namaArray[i] = scanner.nextLine();
            System.out.print("Masukkan Nilai Kehadiran: ");
            double nilaiKehadiran = scanner.nextDouble();
            System.out.print("Masukkan Nilai Midtest: ");
            double nilaiMidtest = scanner.nextDouble();
            System.out.print("Masukkan Nilai UAS: ");
            double nilaiUAS = scanner.nextDouble();

            double nilaiAkhir = (0.2 * nilaiKehadiran) + (0.4 * nilaiMidtest) + (0.4 * nilaiUAS);
            nilaiAkhirArray[i] = nilaiAkhir;

            char grade = hitungGrade(nilaiAkhir);
            gradeArray[i] = grade;

            if (grade == 'A' || grade == 'B' || grade == 'C') {
                jumlahLulus++;
            } else {
                jumlahTidakLulus++;
            }
        }

        System.out.println("\n================================================================");
        System.out.printf("%-4s%-12s%-12s%-12s%-6s%s\n", "No.", "NIM", "Nama", "Nilai Akhir", "Grade", "");
        System.out.println("================================================================");

        for (int i = 0; i < jumlahMahasiswa; i++) {
            System.out.printf("%-4d%-12d%-12s%-12.2f%-6s\n", (i + 1), nimArray[i], namaArray[i], nilaiAkhirArray[i], gradeArray[i]);
        }

        System.out.println("\n================================================================");
        System.out.println("Jumlah Mahasiswa : " + jumlahMahasiswa + " (berdasarkan hasil kalkulasi)");
        System.out.println("Jumlah Mahasiswa yg Lulus : " + jumlahLulus + " (berdasarkan hasil kalkulasi)");
        System.out.println("Jumlah Mahasiswa yg Tidak Lulus : " + jumlahTidakLulus + " (berdasarkan hasil kalkulasi)");
        System.out.println("================================================================");

        scanner.close();
    }

    private static char hitungGrade(double nilaiAkhir) {
        if (nilaiAkhir >= 85 && nilaiAkhir <= 100) {
            return 'A';
        } else if (nilaiAkhir >= 76 && nilaiAkhir <= 84) {
            return 'B';
        } else if (nilaiAkhir >= 61 && nilaiAkhir <= 75) {
            return 'C';
        } else if (nilaiAkhir >= 46 && nilaiAkhir <= 60) {
            return 'D';
        } else {
            return 'E';
        }
    }
}
